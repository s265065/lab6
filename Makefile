all: build

build: main.c bmp.c bmp.h image.c image.h
	gcc -o main -ansi -std=c99 -pedantic -Wall -Werror main.c bmp.c image.c
clean:
	rm -f main

