#pragma once

#include <stdint.h>
#include "bmp.h"

struct pixel {
    uint8_t red;
    uint8_t green;
    uint8_t blue;
};

struct image {
    uint32_t width;
    uint32_t height;

    struct pixel * pixels;
};

struct image image_from_bmp(struct bmp_image *bmp_image);

struct image rotate(const struct image image, int angle);

void bmp_from_image(struct bmp_image * bmp_image, const struct image image);