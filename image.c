#include "image.h"

#include <stdlib.h>
#include <string.h>

struct image image_from_bmp(struct bmp_image *bmp_image) {
    struct image image;
    uint32_t size = bmp_image->header.biWidth * bmp_image->header.biHeight;

    image.width = bmp_image->header.biWidth;
    image.height = bmp_image->header.biHeight;
    image.pixels = malloc(sizeof(struct pixel) * image.width * image.height);

    uint32_t i;
    for (i = 0; i < size; ++i) {
        image.pixels[i].red = bmp_image->pixels[i].r;
        image.pixels[i].green = bmp_image->pixels[i].g;
        image.pixels[i].blue = bmp_image->pixels[i].b;
    }


    return image;
}

struct image rotate(const struct image image, int angle){
    struct image new_image;

    new_image.pixels = malloc(sizeof(struct pixel) * image.width * image.height);

    uint32_t x;
    uint32_t y;

    while (angle < 0) {
        angle += 360;
    }

    angle %= 360;
    switch (angle){
        case 90:
            new_image.width = image.height;
            new_image.height = image.width;
            for (y = 0; y < image.height; ++y) {
                for (x = 0; x < image.width; ++x) {
                    new_image.pixels[x * image.height + y] = image.pixels[(image.height - y - 1) * image.width + x];
                }
            }
            break;

        case 180:
            new_image.width = image.width;
            new_image.height = image.height;
            for (y = 0; y < image.height; ++y) {
                for (x = 0; x < image.width; ++x) {
                    new_image.pixels[x * image.height + y] = image.pixels[(image.width - x -1)* image.height + (image.height - y -1)];
                }
            }
            break;

        case 270:
            new_image.width = image.height;
            new_image.height = image.width;
            for (y = 0; y < image.height; ++y) {
                for (x = 0; x < image.width; ++x) {
                    new_image.pixels[x * image.height + y] = image.pixels[y * image.width + (image.width - x - 1)];
                }
            }
            break;

        default:
            new_image.width = image.width;
            new_image.height = image.height;
            memcpy(new_image.pixels, image.pixels, sizeof(struct pixel) * new_image.width * new_image.height);
            break;
    }

    return new_image;

}

void bmp_from_image(struct bmp_image * bmp_image, const struct image image) {

    bmp_image->header.bfType[0] = 'B';
    bmp_image->header.bfType[1] = 'M';

    bmp_image->header.bfOffBits = sizeof(struct bmp_header);

    bmp_image->header.biSize = 40;
    bmp_image->header.biPlanes = 1;
    bmp_image->header.biBitCount = 24;
    bmp_image->header.biCompression = 0;

    bmp_image->header.biWidth = image.width;
    bmp_image->header.biHeight = image.height;

    bmp_image->header.biSizeImage = bmp_image->header.biHeight *
                                    (bmp_image->header.biWidth * sizeof(struct bmp_pixel)
                                     + bmp_image->header.biWidth % 4);

    bmp_image->header.bfSize = bmp_image->header.bfOffBits + bmp_image->header.biSizeImage;

    uint32_t i;
    for (i = 0; i <  bmp_image->header.biHeight*bmp_image->header.biWidth; ++i) {
        bmp_image->pixels[i].b = image.pixels[i].blue ;
        bmp_image->pixels[i].g = image.pixels[i].green;
        bmp_image->pixels[i].r = image.pixels[i].red;
    }

}