#include <stdlib.h>
#include <stdint.h>

#include "bmp.h"
#include "image.h"


int main(int argc, char **argv) {
    int i;

    for (i = 0; i < argc; i++) {
        printf("%s\n", argv[i]);
    }

    char * input_filename = argv[1];
    int angle;
    sscanf(argv[2], "%d", &angle );
    char * output_filename = argv[3];

    FILE * file;
    struct bmp_image *bmp_image = malloc(sizeof(struct bmp_image));

    file = fopen(input_filename, "rb");
    bmp_read(bmp_image, file);
    fclose(file);

    struct image image = image_from_bmp(bmp_image);
    struct image rotated_image = rotate(image, angle);

    bmp_from_image(bmp_image, rotated_image);

    file = fopen(output_filename, "wb");
    bmp_write(*bmp_image, file);

    return 0;
}
